let Hamburger = function (size, stuffing) {
    try {
        if (!size) {
            throw new HamburgerException ("Choose the correct size");
        }
    if (!stuffing) {
        throw  new HamburgerException("Stuffing is not correct");
    }
    if (size.type !== 'size') {
      throw new HamburgerException('Wrong size')
    }
    if (stuffing.type !== "stuffing") {
        throw new HamburgerException("Wrong stuffing type")
     }
    }
    catch (error) {
        console.log(error.name, error.message)
    }

    this.size = size;
    // this.prise = 0; Getter\setter

    this.stuffing = stuffing;

    this.topping = [];
};
Hamburger.prototype =  {};
Hamburger.prototype.calculateCalories = function () {
    let toppingCalories = 0;
    this.topping.forEach(item => toppingCalories += item.calories);
    return this.size.calories + this.stuffing.calories + toppingCalories
};
Hamburger.prototype.calculatePrice = function () {
    let toppingPrice = 0;
    this.topping.forEach(item => toppingPrice += item.price);
   return this.size.price + this.stuffing.price + toppingPrice
};
Hamburger.prototype.getSize = function () {
    return this.size;
};
Hamburger.prototype.getToppings = function () {
    try {
        if (this.topping.includes(topping)) {
            throw new HamburgerException("topping already added")
        }

        this.topping.push(topping);

    } catch (error) {
        console.log(error.name, error.message)
    }
    return this.topping;
};
Hamburger.prototype.getStuffing = function () {
    return this.stuffing;
};
Hamburger.prototype.addTopping = function (topping) {
    this.topping.push(topping);
};

Hamburger.prototype.removeTopping = function (topping) {
    const findTopping = this.topping.find(item => item === topping);
    const index = this.topping.indexOf(findTopping);

    return this.topping.splice(index, 1);
};

Hamburger.SIZE_SMALL = {
    price : 50,
    calories : 20,
    type: 'size'
};
Hamburger.SIZE_LARGE = {
    price : 100,
    calories : 40,
    type: 'size'
};
Hamburger.STUFFING_CHEESE = {
    price : 10,
    calories : 20,
    type: 'stuffing'
};
Hamburger.STUFFING_SALAD = {
    price : 20,
    calories : 5,
    type: 'stuffing'
};
Hamburger.STUFFING_POTATO = {
    price : 15,
    calories : 10,
    type: 'stuffing'
};
Hamburger.TOPPING_MAYO = {
    price : 5,
    calories : 20
};
Hamburger.TOPPING_SPICE = {
    price: 10,
    calories: 20
};
function HamburgerException (message) {
    this.message = message;
    this.name = 'Hamburger error is ';
}

Hamburger.CoockingBook = {};
Hamburger.CoockingBook[Hamburger.STUFFING_CHEESE] = {
    prise : 10,
    calories : 20,
};
Hamburger.CoockingBook[Hamburger.STUFFING_SALAD] = {
    prise : 20,
    calories : 5,
};
Hamburger.CoockingBook[Hamburger.STUFFING_POTATO] = {
    prise : 15,
    calories : 10,
};
Hamburger.CoockingBook[Hamburger.TOPPING_MAYO] = {
    prise : 20,
    calories : 5,
};
Hamburger.CoockingBook[Hamburger.TOPPING_SPICE] = {
    prise : 15,
    calories : 0,
};

let a = new Hamburger(Hamburger.SIZE_SMALL,Hamburger.STUFFING_SALAD );
let e = new Hamburger(Hamburger.STUFFING_CHEESE, Hamburger.SIZE_SMALL );
let b = new Hamburger(Hamburger.SIZE_LARGE,Hamburger.STUFFING_SALAD );
console.log(a.calculateCalories());
console.log(a.calculatePrice());
console.log(a.getSize());
console.log(a.getStuffing());
console.log(a.getToppings());

a.addTopping(Hamburger.TOPPING_MAYO);
a.addTopping(Hamburger.TOPPING_SPICE);
a.removeTopping(Hamburger.TOPPING_MAYO);

